FROM microsoft/iis

SHELL ["powershell"]

RUN Install-WindowsFeature NET-Framework-45-ASPNET ; \
    Install-WindowsFeature Web-Asp-Net45

COPY ./WebApplication1/ WebApplication1
WORKDIR /app
RUN Remove-Website -Name 'Default Web Site'
Run New-Website -name aspnetsite1 -port 80 \
    -PhysicalPath 'C:\WebApplication1' -ApplicationPool '.NET v4.5'

EXPOSE 80

CMD ["ping", "-t", "localhost"]